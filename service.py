import inspect
import multiprocessing
from datetime import datetime

from packstorm.modeltools.concurrent import ModelApi

from util import utils as Util

locales = Util.list_coordinates()

start_time = datetime.now()
Util.print_message(tag='A', message="Inicinando o processo", clss=__name__, met=__name__)

config = Util.get_json_file("config/main.json")

for group in config:
    
    for item in config[group]:
        Util.print_message(tag='I', message=f"Lendo arquivo de {group} para {item['output_name']}", clss=__name__, met=__name__)
        # data = ModelApi(f"/data/weathermaps/forecast/WRF/009/M0/00/hourly/{item['file']}")
        
        for idlocale in locales:
            if group not in locales[idlocale]:
                locales[idlocale][group] = dict()
            
            try:
                locales[idlocale][group][item['output_name']] = dict()

                times, values = Util.get_data(
                    file_or_buffer=f"/data/weathermaps/forecast/WRF/009/M0/00/hourly/{item['file']}",
                    longitude=locales[idlocale]['coords'][0],
                    latitude=locales[idlocale]['coords'][1],
                    variable=item['variable'],
                    use_nearest_point=False,
                    date_begin=None,
                    max_distance=1,
                    date_end=None,
                    level=None
                )
                values = list(map(float, values))
                locales[idlocale][group][item['output_name']] = dict(zip(times, values))
                

                #locales[idlocale][group][item['output_name']][str(data.get_time(time))] = float("{0:.4f}".format(value))

                # y,x = data.near_yx(locales[idlocale]['coords'][1], locales[idlocale]['coords'][0])
                # result = data[item['variable']][:,y,x]


                # for time, value in enumerate(result):
                #     locales[idlocale][group][item['output_name']][str(data.get_time(time))] = float("{0:.4f}".format(value))
            except:
                pass
                # Util.print_message(tag='E', message=f"Erro ao carregar valor {locales[idlocale]['coords']}", clss=__name__, met=inspect.stack()[0][3])
                
Util.print_message(tag='A', message=f"O processo foi finalizado com {(datetime.now()-start_time).total_seconds()} segundos", clss=__name__, met=inspect.stack()[0][3])
Util.save_output(locales)
