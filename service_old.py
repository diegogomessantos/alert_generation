from util import utils_old as Util
from packstorm.modeltools import ModelApi
from datetime import datetime

start_time = datetime.now()


config = Util.get_json_file("config/main.json")
locales = Util.list_coordinates()

Util.print_info("Inicinando o processo")

for group in config:

    for item in config[group]:

        Util.print_info(f"Lendo arquivo de {group} para {item['output_name']}")
        data = ModelApi(f"/data/weathermaps/forecast/WRF/009/M0/00/hourly/{item['file']}")
        
        for idlocale in locales:
            try:
                y,x = data.near_yx(locales[idlocale]['coords'][1], locales[idlocale]['coords'][0])
                result = data[item['variable']][:,y,x]

                locales[idlocale][group] = dict()
                locales[idlocale][group][item['output_name']] = dict()

                for time, value in enumerate(result):
                    locales[idlocale][group][item['output_name']][str(data.get_time(time))] = float("{0:.4f}".format(value))
            except:
                pass
                #Util.print_info(f"Erro ao carregar valor {locales[idlocale]['coords']}",'E')
                

Util.print_info(f"O processo foi finalizado com {(datetime.now()-start_time).total_seconds()} segundos")
Util.save_output(locales)
