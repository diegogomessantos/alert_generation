import inspect
import itertools
import json
import os
import os.path as path
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from glob import glob
from random import randint

import netCDF4
import numpy

from braceexpand import braceexpand
from packstorm.modeltools.concurrent import ModelApi
from packstorm.modeltools.concurrent.utils import (adjust_cut_time,
                                                   datetime_to_float, get_time)
from scipy.spatial.ckdtree import cKDTree


def print_message(message, clss, met, tag='I', error=None):
    '''
    :param message:
    :param clss:
    :param met:
    :param error:
    :param tag:
    :param verbose:
    :return:
    '''

    set_colors = dict(
        I=lambda i: '\033[92m{0}\033[0m'.format(i),
        A=lambda i: '\033[93m{0}\033[0m'.format(i),
        E=lambda i: '\033[91m{0}\033[0m'.format(i),
    )
    tag = tag.upper().strip()

    if tag == 'E':
        _info = '[{tag}.{dt:%Y-%m-%d %H:%M:%S}][PID.{pid}][{clss}.{met}] >> [{_info}] @ [{_error}]'.format(
            tag=set_colors[tag](tag),
            dt=datetime.now(),
            pid=os.getpid(),
            clss=clss,
            met=met,
            _info=message,
            _error=error,
        )

    else:
        _info = '[{tag}.{dt:%Y-%m-%d %H:%M:%S}][PID.{pid}][{clss}.{met}] >> [{_info}]'.format(
            tag=set_colors[tag](tag),
            dt=datetime.now(),
            pid=os.getpid(),
            clss=clss,
            met=met,
            _info=message,
        )

    print(_info)

def save_output(config):
    '''
    :param message: Save a output json to Geoalerta
    '''

    output = open('output/automatic_alert.json', 'w')
    output.write(json.dumps(config)) 

def get_json_file(file_path):
    '''
    :param message: Read Json configuration with variables and outputs
    '''
    print_message(tag='I', message='Lendo as configurações principais', clss=__name__, met=__name__)
    
    try:
        return json.load(open(file_path))
    except:
        print(f"Can't find json file: {file_path}")


def list_coordinates():
    '''
    :param message: List MOC coordinate for tests
    :return: Return a dic with idlocale, name and coordinates
    '''
    print_message(tag='I', message='Gerando as coordenadas aleatórias',clss=__name__, met=__name__)
    
    xy = dict()
    for lp in range(10000):
        idlocale=randint(1300, 9999)
        xy[idlocale] = {'name':f'Localidade {idlocale}','coords': [randint(-60, -35), randint(-20, 0)]}

    return xy

def pool_filter(pool, func, candidates):
    '''

    :param pool:
    :param func:
    :param candidates:
    :return:
    '''

    return [c for c, keep in zip(candidates, pool.map(func, candidates)) if keep]


def get_near_point_with_value(dataset: numpy.ndarray, missing_value, latitude_array: numpy.ndarray,
                              longitude_array: numpy.ndarray, latitude, longitude, level=None, max_distance=1,
                              **kwargs):
    """

    :param dataset:
    :param missing_value:
    :param latitude_array:
    :param longitude_array:
    :param latitude:
    :param longitude:
    :param level:
    :param max_distance:
    :param kwargs:
    :return:
    """

    near_y = lambda iy: numpy.nanargmin(numpy.abs(latitude_array - iy))
    near_x = lambda iy: numpy.nanargmin(numpy.abs(longitude_array - iy))
    if level is None:
        get_value = lambda p: dataset[0, near_y(p[0]), near_x(p[1])]
    else:
        get_value = lambda p: dataset[0, int(level), near_y(p[0]), near_x(p[1])]

    value = get_value((latitude, longitude))
    if numpy.float16(value) == numpy.float16(missing_value) or numpy.isnan(value):

        points = [(_lat, _lon) for _lat in latitude_array for _lon in longitude_array]

        tree = cKDTree(points)
        distances, indices = tree.query((latitude, longitude), len(points), p=2, distance_upper_bound=max_distance)

        indices = indices[distances != numpy.inf]
        distances = distances[distances != numpy.inf]

        func = lambda point: (point[0], (points[point[1]][0], points[point[1]][1]))

        # with ThreadPoolExecutor(int(kwargs.get('number_processes', 2))) as pool:
        near_points = list(map(func, zip(distances, indices)))

        near_points.sort(key=lambda item: item[0])
        for point in near_points:
            value = get_value(point[1])
            if numpy.float16(value) != numpy.float16(missing_value) and not numpy.isnan(value):
                return point[1]

    return latitude, longitude


def search_files(filepath, cut_list_files=None):
    '''

    :param filepath:
    :param cut_list_files:
    :return:
    '''

    files = list()
    filepath = filepath if isinstance(filepath, list) else [filepath]
    filepath = list(map(braceexpand, filepath))
    filepath = list(itertools.chain.from_iterable(filepath))

    for item in filepath:
        if not bool(cut_list_files):
            files += glob(item)
        else:
            _files = glob(item)
            if bool(_files):
                files += _files[adjust_cut_time(cut_list_files)]
    return files


def get_data_from_netcdf(file_or_buffer, variable, latitude, longitude, level=None, date_begin=None,
                         date_end=None, **kwargs):
    '''

    :param file_or_buffer:
    :param variable:
    :param latitude:
    :param longitude:
    :param level:
    :param date_begin:
    :param date_end:
    :return:
    '''
    if isinstance(file_or_buffer, str) and os.path.exists(file_or_buffer) and ModelApi.is_netcdf(file_or_buffer):
        dataset = netCDF4.Dataset(file_or_buffer, 'r')
    elif isinstance(file_or_buffer, netCDF4.Dataset):
        dataset = file_or_buffer
    else:
        raise Exception(f"{file_or_buffer} isn't NETCDF ")

    labels = dict(
        latitude=['latitude', 'lat', 'xlat', 'LATITUDE'], longitude=['longitude', 'lon', 'xlon', 'LONGITUDE'],
        level=['level', 'lev', 'LEVEL', 'levels', 'LEVELS'], time=['time', 'TIME']
    )

    label_time = list(set(labels['time']).intersection(dataset.variables.keys()))[0]
    label_latitude = list(set(labels['latitude']).intersection(dataset.variables.keys()))[0]
    label_longitude = list(set(labels['longitude']).intersection(dataset.variables.keys()))[0]
    # label_level = list(set(labels['level']).intersection(dataset.variables.keys()))[0]

    _get_var = lambda item: dataset.variables[item]

    time_unit = getattr(_get_var('time'), 'units')
    freq, ref_time = ModelApi.parse_time_units(time_unit)

    near_time = lambda value: numpy.nanargmin(numpy.abs(_get_var(label_time)[:] - value))
    near_y = lambda iy: numpy.nanargmin(numpy.abs(_get_var(label_latitude)[:] - iy))
    near_x = lambda iy: numpy.nanargmin(numpy.abs(_get_var(label_longitude)[:] - iy))

    func_date = lambda item: near_time(
        datetime_to_float(value=item, ref_time=ref_time, freq=freq)) if item is not None else None

    with ThreadPoolExecutor(int(kwargs.pop('number_processes', 2))) as po:
        date_begin, date_end = list(po.map(func_date, [date_begin, date_end]))
        date_end = date_end + 1 if date_end is not None else None
        times = list(map(lambda i: str(get_time(i, time_unit)), _get_var(label_time)[date_begin: date_end]))

    if kwargs.get('close_dataset', False):
        try:
            dataset.close()
        except Exception as error:
            pass

    if level is not None:
        return times, _get_var(variable)[date_begin:date_end, level, near_y(latitude), near_x(longitude)]

    return times, _get_var(variable)[date_begin:date_end, near_y(latitude), near_x(longitude)]


def __get_data_from_netcdf(kwargs: dict):
    return get_data_from_netcdf(**kwargs)


def get_data(file_or_buffer: str, variable: str, latitude: float, longitude: float, level: int = None, date_begin=None,
             date_end=None, use_nearest_point=False, max_distance=1, **kwargs):
    '''

    :param file_or_buffer:
    :param variable:
    :param latitude:
    :param longitude:
    :param level:
    :param date_begin:
    :param date_end:
    :param use_nearest_point:
    :param max_distance: if use_nearest_point is true, default 1
    :return:
    '''

    latitude, longitude = numpy.float64(latitude), numpy.float64(longitude)
    try:
        with ThreadPoolExecutor(int(kwargs.get('number_processes', 2))) as pot:
            c2 = isinstance(file_or_buffer, list) and list(filter(lambda i: isinstance(i, str), file_or_buffer))
            if isinstance(file_or_buffer, str) or c2:
                datasets = search_files(filepath=file_or_buffer, cut_list_files=kwargs.get('cut_list_files'))
                if datasets:
                    datasets = pool_filter(pool=pot, func=ModelApi.is_netcdf, candidates=datasets)
                    if use_nearest_point:
                        tmp_dataset = ModelApi(datasets[0], variables=[variable], cut_time=(0, 1))
                        latitude, longitude = get_near_point_with_value(
                            dataset=tmp_dataset[variable][0:1],
                            missing_value=tmp_dataset.get_complete_variable(variable).missing_value,
                            latitude_array=tmp_dataset.latitude, longitude_array=tmp_dataset.longitude,
                            latitude=latitude, longitude=longitude, level=level,
                            max_distance=max_distance,
                            close_dataset=True
                        )
                        del tmp_dataset

                    datasets = list(pot.map(__get_data_from_netcdf, list(pot.map(
                        lambda item: {'file_or_buffer': item, 'variable': variable, 'latitude': latitude,
                                      'longitude': longitude, 'level': level, 'date_begin': date_begin,
                                      'date_end': date_end}, datasets))))
                    datasets = list(pot.map(lambda it: list(zip(*it)), datasets))
                    datasets = sum(datasets, [])
                    datasets.sort(key=lambda item: item[0])
                    datasets = [next(b) for a, b in itertools.groupby(datasets, lambda y: y[0])]
                    return zip(*datasets)

    except Exception as error:
        pass
        # print_message(tag='E', message='', error=error, clss=__name__, met=inspect.stack()[0][3])

    # print_message(tag='I', message=f'Data not found ... {file_or_buffer}', clss=__name__, met=inspect.stack()[0][3])

    return None, None

# if __name__ == '__main__':
#     filepath = '/.../*nc4'
#     variable = 'temperatura'
#     #  max_distance: if use_nearest_point is true, default 1
#     times, values = get_data(
#         file_or_buffer=filepath, variable=variable, latitude=+23.5, longitude=-46.8, level=None, date_begin=None,
#         date_end=None, use_nearest_point=False, max_distance=1
#     )
