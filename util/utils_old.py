from datetime import datetime
from random import randint
import os.path as path
import json

def save_output(config):
    output = open('output/automatic_alert.json', 'w')
    output.write(json.dumps(config)) 

def get_json_file(file_path):
    print_info("Lendo as configurações principais")
    
    try:
        return json.load(open(file_path))
    except:
        print(f"Can't find json file: {file_path}")


def list_coordinates():
    print_info("Gerando as coordenadas aleatórias")
    
    xy = dict()
    for lp in range(10000):
        idlocale=randint(1300, 9999)
        xy[idlocale] = {'name':f'Localidade {idlocale}','coords': [randint(-60, -35), randint(-20, 0)]}

    return xy
    
def print_info(msg, acrom='I', date_print=datetime.now()):
    print(f"[{acrom}] - {date_print} - {msg}")